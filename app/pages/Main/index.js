import { View } from "react-native";
import React from "react";
import Text from "../../components/Text";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const Tab = createBottomTabNavigator();

import Home from "../Home";
import Profile from "../Profile";
import Task from "../Task";
import Performa from "../Performa";
import { HomeIcon, ProfileIcon, PerformIcon, TaskIcon, AddLeads } from "../../assets/svg";

export default function Main({ navigation, router }) {
  return (
    <View style={{ flex: 1 }}>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarActiveTintColor: "#04325F",
          tabBarInactiveTintColor: "#CED1D4",
          tabBarIcon: ({ focused, color, size }) => {
            let tabsIcon = {
              Home: <HomeIcon fill={color} width={size} height={size} />,
              Profile: <ProfileIcon fill={color} width={size} height={size} />,
              Task: <TaskIcon fill={color} width={size} height={size} />,
              Performa: <PerformIcon fill={color} width={size} height={size} />,
              AddLeads: <AddLeads fill={color} width={100} height={100} />,
            };
            return <View>{tabsIcon[route.name]}</View>;
          },
          tabBarLabel: ({ focused, color }) => (
            <Text bold={focused} fontSize={10} color={color}>
              {route.name}
            </Text>
          ),
          tabBarStyle: {
            paddingBottom: 10,
            paddingTop: 10,
            height: 70,
          },
        })}
      >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Task" component={Task} />
        <Tab.Screen name="AddLeads" component={AddLeads} options={{tabBarLabel:''}}/>
        <Tab.Screen name="Performa" component={Performa} />
        <Tab.Screen name="Profile" component={Profile} />
        
        
      </Tab.Navigator>
    </View>
  );
}
