import { View, ImageBackground, Image } from "react-native";
import React from "react";
import Text from "../../components/Text";
import images from "../../assets/img/index";
import { WIDTH } from "../../assets/styles";

export default function Profile() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground source={images.BG_PROFILE} resizeMode='cover' style={{ width: WIDTH, height: 495, alignItems: 'center', paddingTop: 65, marginBottom: -220 }}>
          <View
            style={{ borderWidth: 2, borderColor: "#FBD2A5", borderRadius: 90 }}
          >
            <Image
              source={images.PERSON2}
              style={{ width: 120, height: 120, borderRadius: 90 }}
            />
          </View>
          <Text fontSize={20} bold style={{ marginTop: 16 }}>
            Nisrina Hanifa Setiono
          </Text>
          <Text bold color="#909090">
            React Native Developer
          </Text>
        </ImageBackground>
         <View style={{
                      width: 400,
                      borderRadius: 12,
                      marginTop: 20,
                      backgroundColor: '#FFF',
                      paddingLeft: 16,
                      paddingRight: 16,
                      shadowColor: '#000',
                      shadowOffset: { width: 0, height: 0.5 },
                      shadowOpacity: 0.15,
                      shadowRadius: 4,
                      elevation: 3
                  }}>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                              <Text bold>ID</Text>
                              <Text color='#A7A7A7'>123456</Text>
                          </View>
                          <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                              <Text bold>Email</Text>
                              <Text color='#A7A7A7'>hallonisrina@gmail.com</Text>
                          </View>
                          <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                              <Text bold>Date Of Birth</Text>
                              <Text color='#A7A7A7'>20 April 2001</Text>
                          </View>
                          <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                              <Text bold>Gender</Text>
                              <Text color='#A7A7A7'>Female</Text>
                          </View>
                          <View style={{ borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                  </View>
                  <View style={{ alignItems: 'center' }}>
                      <View style={{
                          marginTop: 20,
                          width: 400,
                          height: 90,
                          backgroundColor: '#FFF',
                          borderRadius: 12,
                          padding: 16,
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                          shadowColor: '#000',
                          shadowOffset: { width: 0, height: 0.5 },
                          shadowOpacity: 0.15,
                          shadowRadius: 4,
                          elevation: 3
                      }}>
                          <View>
                              <Text bold>Team</Text>
                              <Text color='#A7A7A7'>React Native</Text>
                          </View>
                          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                              <Image source={images.PERSON1} style={{ width: 35, height: 35, borderRadius: 90, borderWidth: 1, borderColor: '#FFF', marginRight: -10 }}></Image>
                              <Image source={images.PERSON2} style={{ width: 35, height: 35, borderRadius: 90, borderWidth: 1, borderColor: '#FFF', marginRight: -10 }}></Image>
                              <Image source={images.PERSON3} style={{ width: 35, height: 35, borderRadius: 90, borderWidth: 1, borderColor: '#FFF', marginRight: -10 }}></Image>
                              <View style={{ width: 35, height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 90, backgroundColor: '#C16262', borderWidth: 1, borderColor: '#FFF' }}>
                                  <Text color='#FFF'>+6</Text>
                              </View>
  
                          </View>
  
                      </View>
                  </View>
                  <View style={{
                      width: 400,
                      borderRadius: 12,
                      marginTop: 20,
                      backgroundColor: '#FFF',
                      paddingLeft: 16,
                      paddingRight: 16,
                      shadowColor: '#000',
                      shadowOffset: { width: 0, height: 0.5 },
                      shadowOpacity: 0.15,
                      shadowRadius: 4,
                      elevation: 3
                  }}>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                              <View style={{ flexDirection: 'row' }}>
                                  <Image source={images.GUARD} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                  <Text bold>Privacy and Security</Text>
                              </View>
                              <Image source={images.ARROW}></Image>
                          </View>
                          <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                              <View style={{ flexDirection: 'row' }}>
                                  <Image source={images.HELP} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                  <Text bold>Help</Text>
                              </View>
                              <Image source={images.ARROW}></Image>
                          </View>
                          <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                              <View style={{ flexDirection: 'row' }}>
                                  <Image source={images.ABOUT} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                  <Text bold>About Us</Text>
                              </View>
                              <Image source={images.ARROW}></Image>
                          </View>
                          <View style={{ borderBottomWidth: 1, borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                      <View style={{ marginTop: 16 }}>
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                              <View style={{ flexDirection: 'row' }}>
                                  <Image source={images.LOGOUT} style={{ width: 24, height: 24, marginRight: 11 }}></Image>
                                  <Text bold>Logout</Text>
                              </View>
                              <Image source={images.ARROW}></Image>
                          </View>
                          <View style={{ borderColor: '#D3D3D3', marginTop: 13 }}></View>
                      </View>
                  </View>
                  <View style={{ marginTop: 20 }}>
                      <Text bold>v0.01</Text>
                  </View>
  
  
              </View>
  
    );
  }
