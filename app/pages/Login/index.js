import {
  View,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
} from "react-native";
import React, { useEffect, useState } from "react";
import Text from "../../components/Text";
import images from "../../assets/img";
import { EyeIcon, EyeSlashIcon } from "../../assets/svg";
import { Fonts, WIDTH, HEIGHT } from "../../assets/styles";
import Satellite from "../../services/satellite";

export default function Login({ navigation, router }) {
  const [isEnable, setIsEnable] = useState(true);
  const [showPass, setShowPass] = useState(true);
  const [email,setEmail]=useState("");
  
  const [password,setPassword] = useState("");
  const [errorEmail, setErrorEmail]= useState("");
  const [errorPassword, setErrorPassword]= useState("");
  const[isLoading, setIsLoading]=useState(false);

  useEffect(()=> {
    // console.log("EMAIL", email);
    // console.log("PASS", password);
    // console.log("EMAIL ERR", errorEmail);
    // console.log("PASS ERR", errorPassword);
    
    if(
      email !=="" && 
      password !==" "&& 
      errorEmail === ""&& 
      errorPassword===""){
        setIsEnable(false);
      }else{
        setIsEnable(true);
    }
  },[email,password,errorEmail,errorPassword])

  const onSubmit = async() =>{
    setIsLoading(true);
    const body={
      email:email,
      password:password,
  };
  console.log("BODY",JSON.stringify(body,null,2));

  // try{
  //   const response = await Satellite.post("auth/login",body);
  //   console.log("RESPONSE",response.data);
  // }catch (err){
  //   console.log("ERROR",err);
  // }finally{
  //   console.log("FINAL")
  // }
  // console.log("RESPONSE",response);
  let ress;
  await Satellite.post("auth/login",body)
  .then((response)=>{
    console.log("RESPONSE",response.data);
    ress= response.data;
    navigation.navigate("Main");
  })
  .catch((error)=>{
    console.log("ERROR",error);
    setErrorPassword("Invalid Email or Password")
  })
  .finally(()=>{
    console.log("FINALLY");
    // setTimeout(() =>{
      setIsLoading(false);
    // },1000);
    
  });

  console.log("PALING BAWAH",ress);

  // const response = await Satellite.post("auth/login",body)
  // console.log("RESPONSE", response);
};


  return (
    <ImageBackground
      source={images.BG_AUTH}
      resizeMode="cover"
      style={{ width: WIDTH, height: HEIGHT }}
    >
      <View
        style={{
          marginTop: 90,
          marginHorizontal: 16,
          // borderWidth:1,
          // borderColor: "#FFF"
        }}
      >
        {/* Email Section */}
        <View>
          <Text color={"#FFF"} fontSize={16} bold>
            Email
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
            }}
          >
            <TextInput
              placeholder={"Enter Your Email"}
              placeholderTextColor={"#D3D3D3"}
              keyboardType={"email-address"}
              autoCapitalize={"none"}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize:16,
              }}
              onChangeText={(value)=>{
                setEmail(value);
                const emailRegex=
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                
                if(value===""){
                  setErrorEmail("email must be filled in");
                  return;
                }
                if (!emailRegex.test(value)){
                  setErrorEmail("invalid mail address");
                  return;
                }

                setErrorEmail("");
                console.log("VALUE",value);
              }}
            />
          </View>
          <Text color="#EA8685" style ={{ textAlign:"right",marginRight:8}}>
            {errorEmail}
          </Text>
        </View>
        {/* Password Section */}
        <View style={{ marginTop: 0}}>
          <Text color={"#FFF"} fontSize={16} bold>
            Password
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <TextInput
              value={password}
              placeholder={"Password"}
              secureTextEntry={showPass}
              placeholderTextColor={"#D3D3D3"}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16,
                flex: 1,
              }}
              onChangeText={(value)=>{
                setPassword(value);
                  if(value === ""){
                    setErrorPassword("password must be filled in");
                    return;
                  }
                  setErrorPassword("");
              }
            }
            />
            <TouchableOpacity onPress={() => setShowPass(!showPass)}>
              {showPass ? (
                <EyeIcon width={20} height={20} stroke={"#FFF"} />
              ) : (
                <EyeSlashIcon width={20} height={20} stroke={"#FFF"} />
              )}
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row", marginTop: 5 }}>
          <Text
            fontSize={10}
            color={"#F6E58D"}
            bold
            style={{ marginTop: 5, marginLeft: 5 }}
          >
            Forgot Password?
          </Text>
          <Text
              color="#EA8685"
              fontSize={10}
              style={{ flex: 1, textAlign: "right", marginRight: 8 }}
            >
              {errorPassword}
            </Text>
          </View>
        </View>
        {/* Button Submit */}
        <TouchableOpacity
          disabled={isEnable || isLoading}
          onPress={onSubmit}
          style={{
            backgroundColor: "#18DCFF",
            borderRadius: 8,
            paddingVertical: 12,
            alignItems: "center",
            marginTop: 20,
            opacity: isEnable || isLoading? 0.5 : 1,
          }}
        >
        {isLoading ?(
          <ActivityIndicator size="small" color="#261A31"/>
        ):(
          <Text color={"#261A31"} fontSize={16} bold>
            Login
          </Text>
          )}
        </TouchableOpacity>

        <View style={{ alignItems: "center", marginTop: 20 }}>
          <Text color={"#FFF"} bold>
            Dont Have an Account?{" "}
            <Text bold color={"#F6E58D"} onPress={() => navigation.navigate ("Register")}>
              Sign Up
            </Text>
          </Text>
        </View>
      </View>
    </ImageBackground>
  );
}
