import {
    View,
    ImageBackground,
    ActivityIndicator,
    TouchableOpacity,
    TextInput,
  } from "react-native";
  import React, { useEffect, useState } from "react";
  import Text from "../../components/Text";
  import images from "../../assets/img";
  import { EyeIcon, EyeSlashIcon } from "../../assets/svg";
  import { Fonts, WIDTH, HEIGHT } from "../../assets/styles";
  import Satellite from "../../services/satellite";
import { ScrollView } from "react-native-gesture-handler";
  
  export default function Register({ navigation, router}) {
    const [isEnable, setIsEnable] = useState(true);
    const [showPass, setShowPass] = useState(true);
    const [showConfirmPass, setShowConfirmPass] = useState(true);
    const [name, setName] = useState("");
    const [errorName, setErrorName] = useState("");
    const [email, setEmail] = useState("");
    const [errorEmail, setErrorEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [errorPhone, setErrorPhone] = useState("");
    const [nik, setNik] = useState("");
    const [errorNik, setErrorNik] = useState("");
    const [password, setPassword] = useState("");
    const [errorPassword, setErrorPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [errorConfirmPassword, setErrorConfirmPassword] = useState("");
    const[isLoading, setIsLoading]=useState(false);
    
    useEffect(()=> {
        if(
          name !=="" && 
          email !=="" && 
          phone !=="" &&
          nik !=="" &&
          password !==" "&&
          confirmPassword !=="" && 
          errorName === ""&& 
          errorEmail === ""&& 
          errorPhone === ""&& 
          errorNik === ""&& 
          errorPassword === ""&& 
          errorConfirmPassword===""){
            setIsEnable(false);
          }else{
            setIsEnable(true);
        }
      },[name,
        email,
        phone,
        nik,
        password,
        confirmPassword,
        errorName,
        errorEmail,
        errorPhone,
        errorNik,
        errorPassword,
        errorConfirmPassword,])
    const handleRegister = async() =>{
        setIsLoading(true);
        const body={
            doSendRegister:{
            name:name,
            email:email,
            phoneNumber:phone,
            nik:nik,
            password:password,
         }
      };
    console.log("BODY",JSON.stringify(body,null,2));

    // let ress;
    await Satellite.post("auth/register",body)
    .then((response)=>{
        if (response.status===200){
        console.log("RESPONSE",response.data);
        // ress= response.data;
        navigation.navigate("Login");
        }else{
            console.log("ERROR",error);
            setErrorEmail("Email has been register");
        }
    })
    .catch((error)=>{
        console.log("ERROR",error);
        setErrorEmail("Email has been register");
    })
    .finally(()=>{
        console.log("FINALLY");
        // setTimeout(() =>{
        setIsLoading(false);
        // },1000);
        
    });
  };
  return (
    <ImageBackground
      source={images.BG_AUTH}
      resizeMode="cover"
      style={{ width: WIDTH, height: HEIGHT }}
    >
    <ScrollView>
      <View
        style={{
          marginTop: 90,
          marginHorizontal: 16,
        }}
      >
        {/* Nama Section */}
        <View>
          <Text color={"#FFF"} fontSize={16} bold>
            Nama
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
              borderColor: errorName? '#EA8585':'#132040',
            }}
          >
            <TextInput
              placeholder={"Masukkan Nama"}
              placeholderTextColor={"#D3D3D3"}
              autoCapitalize={"words"}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16,
              }}
              onChangeText={(value) =>{ setName(value);
                if(value===""){
                    setErrorName("name must be filled in");
                    return;
                  }
                  setErrorName("");
                  console.log("VALUE",value);
                }
            }
            />
          </View>
          <Text color="#EA8685" style ={{ textAlign:"right",marginRight:8}}>
            {errorName}
          </Text>
        </View>
        {/* Email Section */}
        <View style={{ marginTop: 16 }}>
          <Text color={"#FFF"} fontSize={16} bold>
            Email
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
              borderColor: errorEmail? '#EA8585':'#132040',
            }}
          >
            <TextInput
              placeholder={"Masukkan Email"}
              placeholderTextColor={"#D3D3D3"}
              keyboardType={"email-address"}
              autoCapitalize={"none"}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16,
              }}
              onChangeText={(value)=>{
                setEmail(value);
                const emailRegex=
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                
                if(value===""){
                  setErrorEmail("email must be filled in");
                  return;
                }
                if (!emailRegex.test(value)){
                  setErrorEmail("invalid mail address");
                  return;
                }
                setErrorEmail("");
                console.log("VALUE",value);
              }}
            />
          </View>
          <Text color="#EA8685" style ={{ textAlign:"right",marginRight:8}}>
            {errorEmail}
          </Text>
        </View>

        {/* Phone Section */}
        <View style={{ marginTop: 16 }}>
          <Text color={"#FFF"} fontSize={16} bold>
            Phone
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
              borderColor: errorPhone? '#EA8585':'#132040',
            }}
          >
            <TextInput
              placeholder={"Masukkan Telepon"}
              placeholderTextColor={"#D3D3D3"}
              keyboardType={"phone-pad"}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16,
              }}
              onChangeText={(value)=>{
                setPhone(value);
                const phoneRegex= /^[0-9]+$/;
                if(value===""){
                  setErrorPhone("phone numbers must be filled in");
                  return;
                }
                if (!phoneRegex.test(value)){
                  setErrorEmail("phone only contain numbers");
                  return;
                }
                setErrorPhone("");
                console.log("VALUE",value);
              }}
            />
          </View>
          <Text color="#EA8685" style ={{ textAlign:"right",marginRight:8}}>
            {errorPhone}
          </Text>
        </View>

        {/* NIK Section */}
        <View style={{ marginTop: 16 }}>
          <Text color={"#FFF"} fontSize={16} bold>
            NIK
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
              borderColor: errorNik? '#EA8585':'#132040',
            }}
          >
            <TextInput
              placeholder={"Masukkan NIK"}
              placeholderTextColor={"#D3D3D3"}
              keyboardType={"numeric"}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16,
              }}
              onChangeText={(value)=>{
                setNik(value);
                const nikRegex= /^[0-9]+$/;
                if(value===""){
                  setErrorNik("NIK must be filled in");
                  return;
                }
                if (!nikRegex.test(value)){
                  setErrorEmail("NIK only contain numbers");
                  return;
                }
                if(value.length!==16){
                    setErrorNik("NIK must be 16 digits");
                    return;
                  }
                setErrorNik("");
                console.log("VALUE",value);
              }}
            />
          </View>
          <Text color="#EA8685" style ={{ textAlign:"right",marginRight:8}}>
            {errorNik}
            </Text>
        </View>

        {/* Password Section */}
        <View style={{ marginTop: 16 }}>
          <Text color={"#FFF"} fontSize={16} bold>
            Password
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems:"center",
              borderColor: errorPassword? '#EA8585':'#132040',
            }}
          >
            <TextInput
              placeholder={"Masukkan Password"}
              placeholderTextColor={"#D3D3D3"}
              secureTextEntry={showPass}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16,
              }}
              onChangeText={(value)=>{
                setPassword(value);
                  if(value === ""){
                    setErrorPassword("password must be filled in");
                    return;
                  }
                  setErrorPassword("");
              }
            }
            />
            <TouchableOpacity onPress={() => setShowPass(!showPass)}>
              {showPass ? (
                <EyeIcon width={20} height={20} stroke={"#FFF"} />
              ) : (
                <EyeSlashIcon width={20} height={20} stroke={"#FFF"} />
              )}
            </TouchableOpacity>
          </View>
          <Text color="#EA8685" style ={{ textAlign:"right",marginRight:8}}>
            {errorPassword}
          </Text>
        </View>
        {/* Confirm Password Section */}
        <View style={{ marginTop: 16 }}>
          <Text color={"#FFF"} fontSize={16} bold>
            Confirm Password
          </Text>
          <View
            style={{
              borderWidth: 1,
              marginTop: 8,
              borderColor: "#132040",
              borderRadius: 8,
              padding: 12,
              backgroundColor: "#273C75",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems:"center",
              borderColor: errorConfirmPassword? '#EA8585':'#132040',
            }}
          >
            <TextInput
              placeholder={"Konfirmasi Password"}
              placeholderTextColor={"#D3D3D3"}
              secureTextEntry={showConfirmPass}
              style={{
                color: "#FFF",
                fontFamily: Fonts.Nunito.Bold,
                fontSize: 16,
              }}
              onChangeText={(value)=>{
                setConfirmPassword(value);
                  if(value === ""){
                    setErrorConfirmPassword("confirm your password");
                    return;
                  }
                  if(value !== password ){
                    setErrorConfirmPassword("password doesn't match");
                    return;
                  }
                  setErrorConfirmPassword("");
              }
            }
            />
            <TouchableOpacity onPress={() => setShowConfirmPass(!showConfirmPass)}>
              {showConfirmPass ? (
                <EyeIcon width={20} height={20} stroke={"#FFF"} />
              ) : (
                <EyeSlashIcon width={20} height={20} stroke={"#FFF"} />
              )}
            </TouchableOpacity>
          </View>
          <Text color="#EA8685" style ={{ textAlign:"right",marginRight:8}}>
            {errorConfirmPassword}
          </Text>
        </View>
        {/* Button Register */}
        <TouchableOpacity
          onPress={handleRegister}
          disabled={isEnable || isLoading}
          style={{
            backgroundColor: "#18DCFF",
            borderRadius: 8,
            paddingVertical: 12,
            alignItems: "center",
            marginTop: 20,
            opacity: isEnable || isLoading? 0.5 : 1,
          }}
        >
        {isLoading ?(
          <ActivityIndicator size="small" color="#261A31"/>
        ):(
          <Text color={"#261A31"} fontSize={16} bold>
            Register
          </Text>
          )}
        </TouchableOpacity>
      </View>
      </ScrollView>
    </ImageBackground>
    
  );
        }

  