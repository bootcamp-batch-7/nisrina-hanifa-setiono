import { View, Text, Image } from "react-native";
import React from "react";
import { HomeIcon } from "../../assets/svg";
import image from "../../assets/img";

export default function Home() {
  return (
    <View>
      <Text>Home</Text>

      <HomeIcon width={120} height={120} fill="#d00" />
      <Image source={image.HOME} style={{ width: 100, height: 100 }} />
    </View>
  );
}
