export default {
  BG_PROFILE: require("./bgProfile.png"),
  PERSON2: require("./person2.png"),
  PERSON1: require("./person1.png"),
  PERSON3: require("./person3.png"),
  PERSON5: require("./person5.png"),
  PERSON8: require("./person8.png"),
  HOME: require("./home.png"),
  HOME_INACTIVE: require("./homeInactive.png"),
  BG_AUTH: require("./bgscreen1.png"),
  GUARD : require('./guard.png'),
  ARROW : require('./Vector.png'),
  HELP : require('./help.png'),
  ABOUT : require('./about.png'),
  LOGOUT : require('./logout.png'),
};


// export default {
//   BG_PROFILE : require('./bgProfile.png'),
//   PERSON : require('./person2.png'),
//   PERSON1 : require('./person1.png'),
//   PERSON2 : require('./person3.png'),
//   PERSON3 : require('./person5.png'),
//   GUARD : require('./guard.png'),
//   ARROW : require('./Vector.png'),
//   HELP : require('./help.png'),
//   ABOUT : require('./about.png'),
//   LOGOUT : require('./logout.png'),
//   HOME : require('./home.png'),
//   TASK : require('./task.png'),
//   PERFORMANCE : require('./perform.png'),
//   PROFILE : require('./profile.png'),
//   ADD: require('./Add.png'),
//   BG_LOGIN: require('./bgscreen_1.png')
// }
