"use client";

import Container from "@components/Container";
import React, {useState} from "react";
import ListUser from "./listUser";

export const metadata =  {
  title: "Users"
  };


export default function Users() {
  const [search, setSearch] = useState("");
  const onSubmited = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  
    const inputResult = event.currentTarget[0] as HTMLInputElement;
  
    console.log("MENCARI", inputResult.value);
    setSearch(inputResult.value);
  };

  return (<Container>
 <div className='min-h-screen flex flex-1 flex-col'>
    <h1 className='text-6xl font-bold'>Cari User</h1>
    <form className="flex rounded-xl mt-5 custom-shadow" onSubmit={onSubmited}>
      <input 
      placeholder="Cari Users"
      className="p-2 rounded-l-xl flex-1 text-black"/>
      <button className="bg-lime-600 rounded-r-xl px-5 py-2">Cari User</button>
    </form>

    <ListUser search={search} />

    </div>

  </Container> ) ; 
}
