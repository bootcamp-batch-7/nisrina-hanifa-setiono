import Container from '@components/Container'
import React from 'react'
import Satellite from '@services/satellite'

async function getUserData(param: string) {
  let res;
await Satellite.get("https://api.github.com/users/" + param)
  .then ((response) => {
    console.log("RESPONSE", response);
    res = response.data;
  })
  .catch((error) => {
    console.log("ERROR DATA", error);
  });
  return res;
};

export default async function UserDetails({params} : {params : {slug: string}}) {
    const data= await getUserData(params.slug) as any;
  return (
    <Container>
  <div className='min-h-screen flex flex-1 justify-center items-center flex-col'>
    <h1 className='text-6xl font-bold'>Detail User : {params.slug}</h1>
    <h1 className='text-6xl font-bold'>ID : {data?.id}</h1>
    <h1 className='text-6xl font-bold'>GIT URL : {data?.html_url}</h1>
    <h1 className='text-6xl font-bold'>Email : {data?.email || "-"}</h1>
    <h1 className="text-6xl font-bold">
    Location : {data?.location || "-"}
    </h1>
   
    </div>
  </Container>
  )
}
